window.globalPopup = new Popup();

new Tabs();

const removeViewport = (function() {
	let mq = window.matchMedia('(min-width: 769px)');
	if(mq.matches) {
		let viewport = document.querySelector('[name="viewport"]');
		viewport.remove();
	}
})();

(function() {

	let dataToggleClass = document.querySelectorAll('[data-toggle-class]');

	dataToggleClass.forEach(function(node) {
		node.addEventListener('click', function() {
			this.classList.toggle('active');
		});
	});

})();
